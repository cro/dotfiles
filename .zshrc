HISTFILE=~/.histfile
HISTSIZE=1001
SAVEHIST=1001
setopt extendedglob autocd autopushd
setopt +o nomatch
unsetopt beep notify
bindkey -e
zstyle :compinstall filename '/home/cro/.zshrc'
autoload -Uz compinit
compinit

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

alias ls="exa"
alias ll="exa -l --git"
alias ltr="exa -l --git --sort=time"
alias vim="vim -p"
alias grep="grep --color=auto --exclude-dir={.git}"
alias -s {txt,md,yml,yaml,toml}=bat
