#! /usr/bin/env gnuplot

set terminal pdf enhanced color size 5,4 font 'Helvetica,16'
set datafile commentschars '#!%'
set key inside top right

set output 'example.pdf'

# x axis
set xlabel 'x'
set format x '%1.0f'

# y axis
set ylabel 'y'
set format y '%1.0f'

# legend
set key center bottom maxcols auto maxrows auto
