#!/usr/bin/env bash

if (( "$#" < 1)); then
  echo usage: $0 arg
  exit
fi

read -p "proceed? [y/n] " -n 1 -r; echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo proceeding
fi
